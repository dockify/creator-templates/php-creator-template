<?php

declare(strict_types=1);

namespace %creator_root_namespace%;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

trait RunsProcess
{
    /**
     * @param array $args
     * @param bool $tty
     * @param int $timeout
     * @return Process
     */
    public function runProcess(array $args, bool $tty = true, int $timeout = 600): Process
    {
        $process = new Process($args);
        $process->setTty($tty);
        $process->setTimeout($timeout);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process;
    }
}
