<?php

declare(strict_types=1);

namespace %creator_root_namespace%\Decorators;

use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractDecorator
{
    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * @var bool
     */
    protected $optional;

    /**
     * @var string
     */
    protected $confirmationPhrase;

    public function __construct(SymfonyStyle $io, bool $optional = true)
    {
        $this->io = $io;
        $this->optional = $optional;
    }

    /**
     * @return bool
     */
    public function isOptional()
    {
        return $this->optional;
    }

    /**
     * @return string
     */
    public function getConfirmationPhrase(): string
    {
        return $this->confirmationPhrase ?? sprintf(
            'Would you like to run this decorator? (%s)',
            get_called_class()
        );
    }

    abstract public function configure(): void;

    abstract public function run(): void;

    abstract public function about(): void;
}
