<?php

declare(strict_types=1);

namespace %creator_root_namespace%\Decorator;

use %creator_root_namespace%\Installer;
use %creator_root_namespace%\RunsProcess;

class InstallExampleProject extends AbstractDecorator
{
    use RunsProcess;

    const DEFAULT_PROJECT_NAME = '%default_project_name%';

    /**
     * @var string
     */
    private $projectName;

    public function configure(): void
    {
        $this->projectName = $this->io->ask(
            'How should I name your project?',
            self::DEFAULT_PROJECT_NAME
        );

        $this->io->text(sprintf(
            '%s? Sweet name! So let\'s get started.',
            ucfirst($this->projectName)
        ));
    }

    public function run(): void
    {
        $this->io->title('Installing Project');

        $this->installLaravel();
    }

    protected function installLaravel()
    {
        $this->runProcess([
            'composer', 'create-project', '--prefer-dist', 'laravel/laravel',
            $this->projectName
        ]);
    }

    public function about(): void
    {
        $this->io->title('About this project');
    }
}
