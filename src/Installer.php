<?php

declare(strict_types=1);

namespace %creator_root_namespace%;

use %creator_root_namespace%\Decorator\AbstractDecorator;
use Symfony\Component\Console\Style\SymfonyStyle;

class Installer
{
    const PROJECT_DIR = '/app';

    const CONFIRMATION_YES = 'Yes';
    const CONFIRMATION_NO = 'No';
    const CONFIRMATION_ABOUT = 'About';

    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * @var AbstractDecorator[]
     */
    protected $availableDecorators = [];

    /**
     * @var AbstractDecorator[]
     */
    protected $decoratorsToRun = [];

    public function __construct(SymfonyStyle $io)
    {
        $this->io = $io;
    }

    /**
     * @param AbstractDecorator $decorator
     * @return Installer
     */
    public function addDecorator(AbstractDecorator $decorator): Installer
    {
        $this->availableDecorators[] = $decorator;

        return $this;
    }

    public function install()
    {
        chdir(self::PROJECT_DIR);

        $this->configureDecorators();
        $this->runDecorators();
    }

    private function configureDecorators(): void
    {
        foreach ($this->availableDecorators as $decorator) {
            if (!$decorator->isOptional() || $this->shouldUseDecorator($decorator)) {
                $decorator->configure();
                $this->decoratorsToRun[] = $decorator;
            }
        }
    }

    private function runDecorators(): void
    {
        foreach ($this->decoratorsToRun as $decorator) {
            $decorator->run();
        }
    }

    private function shouldUseDecorator(AbstractDecorator $decorator): bool
    {
        $this->io->text($decorator->getConfirmationPhrase());

        do {
            $choice = $this->io->choice('Select action:', [
                self::CONFIRMATION_YES,
                self::CONFIRMATION_NO,
                self::CONFIRMATION_ABOUT
            ], 0);

            if ($choice === self::CONFIRMATION_ABOUT) {
                $decorator->about();
            }
        } while (!in_array($choice, [
            self::CONFIRMATION_YES,
            self::CONFIRMATION_NO,
        ]));

        return $choice === self::CONFIRMATION_YES;
    }
}
