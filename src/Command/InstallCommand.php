<?php

declare(strict_types=1);

namespace %creator_root_namespace%\Command;

use %creator_root_namespace%\Installer;
use %creator_root_namespace%\Decorator\InstallExampleDecorator;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallCommand extends Command
{
    const CMD_NAME = 'Install Laravel';

    public function configure()
    {
        $this->setName(self::CMD_NAME);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        (new Installer($io))
            ->addDecorator(new InstallExampleDecorator($io, false))
            ->install();
    }
}
