all:
	@echo "Help coming soon..."

shell: cmd.sh
	./cmd.sh exec sh

composer-install: cmd.sh
	./cmd.sh exec "composer install"

init: composer-install
	@echo "Initializing your new dockify creator"

run: cmd.sh
	./cmd.sh exec "/data/application.php run"

build: cmd.sh
	./cmd.sh build

phpstan: cmd.sh
	./cmd.sh exec "composer run-script phpstan"

phpcs: cmd.sh
	./cmd.sh exec "composer run-script phpcs"

phpcbf: cmd.sh
	./cmd exec "composer run-script phpcbf"

quiality-check: phpstan phpcs
	@echo "Running static analysis checks"