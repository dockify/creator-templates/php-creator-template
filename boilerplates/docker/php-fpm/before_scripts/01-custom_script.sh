#!/bin/sh

# Before scripts will run right before php-fpm process starts.
#
# That's a great way to execute required runtime setup such as running worker process,
# loading cron daemon etc.
#
# Before scripts are prefixed with number that represent their execution order.