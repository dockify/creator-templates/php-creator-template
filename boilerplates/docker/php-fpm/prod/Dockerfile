# Clenup dev dependencies before building production image

# If you don't use gitlab CI configured by Dockify, you need
# to pass this variable manually during building image.
ARG PROJECT_REGISTRY_URL

FROM $PROJECT_REGISTRY_URL/php-fpm:build as build

WORKDIR /data
COPY . /data

RUN composer --version \
    && composer install \
        -o \
        --prefer-dist \
        --no-suggest \
        --no-dev \
        --no-scripts \
        --no-progress

# Build production image
FROM registry.gitlab.com/dc-images/php/7.3-fpm-alpine/base:v2.4.1

# This is shared between dev, build and prod images
RUN /common_setup.sh
COPY ./docker/php-fpm/common_setup.sh /common_setup.sh

# Other configuration - dependes on tools used
COPY ./docker/php-fpm/config/cron/crontab /crontab
COPY ./docker/php-fpm/before_scripts/ /before_scripts/custom/
COPY ./docker/php-fpm/config/supervisor/supervisord.conf /etc/supervisord.conf

# This is unique for production image
RUN echo "Running production specific setup"

# Copy app source code
COPY --chown=www-data:www-data --from=build /data /data