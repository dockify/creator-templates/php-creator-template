#!/bin/sh

# It's convenient to have separate dockerfiles for development and production builds.
# On the other hand that leads to code duplication if you need some shared libraries or functionality.
# Common setup script is a way to share those pieces of code to keep everything DRY.

echo "Installing common dependencies used both by prod and dev image" \
    && docker-php-ext-install pcntl \
    && apk add --no-cache \
        dcron \
        supervisor \
    && mkdir -m 0644 -p /var/log \
    && touch /var/log/cron.log
